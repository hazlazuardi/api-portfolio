from django.db import models

# Personal
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User

# Create your models here.

# Model for todo

class Todo(models.Model):
  title = models.CharField(max_length=120)
  owner = models.ForeignKey('auth.User', related_name='todos', on_delete=models.CASCADE)
  description = models.TextField()
  completed = models.BooleanField(default=False)
  online = models.BooleanField(default=True)

  def __str__(self):
    return self.title






