# backend/urls.py

from django.contrib import admin
from django.urls import path, include                 # add this

urlpatterns = [
    path(r'users/', views.UserList.as_view()),
    path(r'users/<int:pk>', views.UserDetail.as_view())
]