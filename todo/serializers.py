# todo/serializers.py

from rest_framework import serializers
from .models import Todo
from django.contrib.auth.models import User
from .models import Todo
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

class UserSerializer(serializers.ModelSerializer):
  todos = serializers.PrimaryKeyRelatedField(many=True, queryset=Todo.objects.all())
  class Meta:
    model = User
    fields = ('id', 'username', 'todos')


class TodoSerializer(serializers.ModelSerializer):
  owner = serializers.ReadOnlyField(source='owner.username')
  class Meta:
    model = Todo
    fields = ('id', 'title', 'owner',  'description', 'completed', 'online')
    extra_kwargs = {
      'owner': {'required': True}
    }



