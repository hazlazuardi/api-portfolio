# backend/urls.py

from django.contrib import admin
from django.urls import path, include                 # add this
from rest_framework.routers import DefaultRouter
from rest_framework_jwt.views import obtain_jwt_token
from todo.views import UserList, UserDetail, TodoList, TodoDetail

router = DefaultRouter()

urlpatterns = [
    path('admin/', admin.site.urls),         
    path(r'', include(router.urls)),
    path(r'api/', include('rest_framework.urls', namespace='rest_framework')),
    path(r'api/users/', UserList.as_view()),
    path(r'api/todos/', TodoList.as_view()),
    path(r'api/todos/<int:pk>/', TodoDetail.as_view()),
    path(r'api/users/<int:pk>/', UserDetail.as_view()),
    path('api-token-auth/', obtain_jwt_token, name='api-token-auth'),
    path('social/', include('social_app.urls')),
]